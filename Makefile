DESTDIR =
SHAREDIR = $(DESTDIR)/usr/share/eyes17
BINDIR = $(DESTDIR)/usr/bin
DESKTOPDIR = $(DESTDIR)/usr/share/applications
ICONDIR = $(DESTDIR)/usr/share/icons
PYTHONDIR = $(DESTDIR)/usr/lib/python3/dist-packages/

all: $(UI_COMPILED)
	$(MAKE) -C layouts all
	$(MAKE) -C lang all
	# generate flags with translation progress status
	python3 language.py

install: all
	install -d $(SHAREDIR)
	cp -Rd *.py blockly code examples helpFiles html \
	    images lang layouts screenshots \
	    $(SHAREDIR)
	install -d $(BINDIR)
	cp bin/eyes17 bin/eyes17-manuals $(BINDIR)
	install -d $(DESKTOPDIR)
	cp desktop/* $(DESKTOPDIR)
	install -d $(ICONDIR)
	cp icons/*.png $(ICONDIR)
	install -d $(PYTHONDIR)
	cp -a eyes17 $(PYTHONDIR)

clean:
	$(MAKE) -C layouts clean
	$(MAKE) -C lang clean
	# remove generated flags with status
	rm -f images/*.status.svg

.PHONY: all install clean
