<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bn_IN" sourcelanguage="en">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../layouts/dio_sensor.ui" line="19"/>
        <source>Sensor Reading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/dio_sensor.ui" line="52"/>
        <source>Data Logger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/dio_control.ui" line="51"/>
        <location filename="../layouts/dio_robot.ui" line="20"/>
        <location filename="../layouts/dio_sensor.ui" line="59"/>
        <source>Initialize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/dio_sensor.ui" line="66"/>
        <source>Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/dio_sensor.ui" line="124"/>
        <source>SINE FIT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/dio_sensor.ui" line="137"/>
        <source>Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/dio_sensor.ui" line="144"/>
        <source>DAMPED SINE FIT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/inputSelector.ui" line="19"/>
        <source>Configure Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/inputSelector.ui" line="71"/>
        <source>REFRESH</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/inputSelector.ui" line="97"/>
        <source>.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/inputSelector.ui" line="128"/>
        <source>CONFIRM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/inputSelector.ui" line="146"/>
        <source>Min </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/inputSelector.ui" line="165"/>
        <source>Max </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/dio_control.ui" line="14"/>
        <source>Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/dio_robot.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/screenshot.ui" line="14"/>
        <source>Screnshot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/screenshot.ui" line="48"/>
        <source>Translations =</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/screenshot.ui" line="61"/>
        <source>Path =</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/screenshot.ui" line="71"/>
        <source>PNG, width =</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/eyes17_manuals.ui" line="14"/>
        <source>Choose the format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/eyes17_manuals.ui" line="20"/>
        <source>Format for the User Manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/eyes17_manuals.ui" line="26"/>
        <source>PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/eyes17_manuals.ui" line="36"/>
        <source>EPUB</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Expt</name>
    <message>
        <location filename="../BHCurve.py" line="40"/>
        <source>Voltage (V) -&gt; Current -&gt; Magnetic Field(B)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BHCurve.py" line="42"/>
        <source>Magnetic Field (H)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BHCurve.py" line="54"/>
        <location filename="../MPU6050.py" line="93"/>
        <location filename="../diodeIV.py" line="53"/>
        <location filename="../drivenRodPendulum.py" line="75"/>
        <location filename="../filterCircuit.py" line="123"/>
        <location filename="../lightsensorlogger.py" line="110"/>
        <location filename="../logger.py" line="104"/>
        <location filename="../npnCEout.py" line="65"/>
        <location filename="../pendulumVelocity.py" line="66"/>
        <location filename="../plotIV.py" line="97"/>
        <location filename="../pnpCEout.py" line="63"/>
        <location filename="../pt100.py" line="153"/>
        <location filename="../rodPendulum.py" line="75"/>
        <location filename="../soundFreqResp.py" line="93"/>
        <location filename="../sr04dist.py" line="75"/>
        <location filename="../sr04dist_fft.py" line="75"/>
        <location filename="../thermocouplelogger.py" line="103"/>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BHCurve.py" line="58"/>
        <location filename="../MPU6050.py" line="97"/>
        <location filename="../diodeIV.py" line="57"/>
        <location filename="../drivenRodPendulum.py" line="79"/>
        <location filename="../filterCircuit.py" line="130"/>
        <location filename="../lightsensorlogger.py" line="114"/>
        <location filename="../logger.py" line="108"/>
        <location filename="../npnCEout.py" line="69"/>
        <location filename="../pendulumVelocity.py" line="70"/>
        <location filename="../plotIV.py" line="101"/>
        <location filename="../pnpCEout.py" line="67"/>
        <location filename="../pt100.py" line="157"/>
        <location filename="../rodPendulum.py" line="79"/>
        <location filename="../soundFreqResp.py" line="103"/>
        <location filename="../sr04dist.py" line="79"/>
        <location filename="../sr04dist_fft.py" line="79"/>
        <location filename="../thermocouplelogger.py" line="107"/>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BHCurve.py" line="62"/>
        <source>Set Zero</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BHCurve.py" line="66"/>
        <location filename="../MPU6050.py" line="101"/>
        <location filename="../diodeIV.py" line="65"/>
        <location filename="../filterCircuit.py" line="134"/>
        <location filename="../induction.py" line="78"/>
        <location filename="../lightsensorlogger.py" line="118"/>
        <location filename="../logger.py" line="112"/>
        <location filename="../npnCEout.py" line="73"/>
        <location filename="../pendulumVelocity.py" line="78"/>
        <location filename="../plotIV.py" line="109"/>
        <location filename="../pnpCEout.py" line="71"/>
        <location filename="../pt100.py" line="161"/>
        <location filename="../soundFreqResp.py" line="107"/>
        <location filename="../sr04dist.py" line="87"/>
        <location filename="../sr04dist_fft.py" line="87"/>
        <location filename="../thermocouplelogger.py" line="111"/>
        <source>Clear Traces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BHCurve.py" line="70"/>
        <location filename="../MPU6050.py" line="105"/>
        <location filename="../RCtransient.py" line="108"/>
        <location filename="../RLCsteadystate.py" line="152"/>
        <location filename="../RLCtransient.py" line="104"/>
        <location filename="../XYplot.py" line="72"/>
        <location filename="../diodeIV.py" line="69"/>
        <location filename="../drivenRodPendulum.py" line="71"/>
        <location filename="../filterCircuit.py" line="138"/>
        <location filename="../induction.py" line="82"/>
        <location filename="../lightsensorlogger.py" line="122"/>
        <location filename="../logger.py" line="116"/>
        <location filename="../npnCEout.py" line="77"/>
        <location filename="../pendulumVelocity.py" line="82"/>
        <location filename="../plotIV.py" line="113"/>
        <location filename="../pnpCEout.py" line="75"/>
        <location filename="../pt100.py" line="165"/>
        <location filename="../rodPendulum.py" line="71"/>
        <location filename="../soundBeats.py" line="129"/>
        <location filename="../soundFreqResp.py" line="111"/>
        <location filename="../soundVelocity.py" line="102"/>
        <location filename="../sr04dist.py" line="96"/>
        <location filename="../sr04dist_fft.py" line="96"/>
        <location filename="../thermocouplelogger.py" line="115"/>
        <source>Save Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BHCurve.py" line="95"/>
        <source>MPU925x Sensor Not Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BHCurve.py" line="122"/>
        <source>Completed plotting B-H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BHCurve.py" line="132"/>
        <source>Residual Magnetic Field: %.2f</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BHCurve.py" line="151"/>
        <location filename="../diodeIV.py" line="162"/>
        <location filename="../npnCEout.py" line="160"/>
        <location filename="../plotIV.py" line="241"/>
        <location filename="../pnpCEout.py" line="158"/>
        <source>Started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BHCurve.py" line="158"/>
        <location filename="../MPU6050.py" line="201"/>
        <location filename="../diodeIV.py" line="169"/>
        <location filename="../drivenRodPendulum.py" line="251"/>
        <location filename="../lightsensorlogger.py" line="212"/>
        <location filename="../logger.py" line="204"/>
        <location filename="../npnCEout.py" line="167"/>
        <location filename="../pendulumVelocity.py" line="196"/>
        <location filename="../plotIV.py" line="252"/>
        <location filename="../pnpCEout.py" line="165"/>
        <location filename="../pt100.py" line="301"/>
        <location filename="../rodPendulum.py" line="220"/>
        <location filename="../soundFreqResp.py" line="263"/>
        <location filename="../sr04dist.py" line="257"/>
        <location filename="../sr04dist_fft.py" line="257"/>
        <location filename="../thermocouplelogger.py" line="210"/>
        <source>User Stopped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BHCurve.py" line="166"/>
        <location filename="../MPU6050.py" line="208"/>
        <location filename="../RLCtransient.py" line="185"/>
        <location filename="../diodeIV.py" line="177"/>
        <location filename="../drivenRodPendulum.py" line="262"/>
        <location filename="../filterCircuit.py" line="325"/>
        <location filename="../induction.py" line="159"/>
        <location filename="../lightsensorlogger.py" line="220"/>
        <location filename="../logger.py" line="212"/>
        <location filename="../npnCEout.py" line="176"/>
        <location filename="../pendulumVelocity.py" line="203"/>
        <location filename="../plotIV.py" line="260"/>
        <location filename="../pnpCEout.py" line="174"/>
        <location filename="../pt100.py" line="309"/>
        <location filename="../rodPendulum.py" line="231"/>
        <location filename="../soundFreqResp.py" line="274"/>
        <location filename="../sr04dist.py" line="264"/>
        <location filename="../sr04dist_fft.py" line="264"/>
        <location filename="../thermocouplelogger.py" line="218"/>
        <source>Cleared Traces and Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BHCurve.py" line="170"/>
        <location filename="../RCtransient.py" line="189"/>
        <location filename="../RLCtransient.py" line="189"/>
        <location filename="../diodeIV.py" line="181"/>
        <location filename="../drivenRodPendulum.py" line="266"/>
        <location filename="../filterCircuit.py" line="332"/>
        <location filename="../induction.py" line="163"/>
        <location filename="../npnCEout.py" line="180"/>
        <location filename="../pendulumVelocity.py" line="207"/>
        <location filename="../plotIV.py" line="264"/>
        <location filename="../pnpCEout.py" line="178"/>
        <location filename="../pt100.py" line="313"/>
        <location filename="../rodPendulum.py" line="235"/>
        <location filename="../soundBeats.py" line="208"/>
        <location filename="../soundVelocity.py" line="181"/>
        <location filename="../sr04dist.py" line="271"/>
        <location filename="../sr04dist_fft.py" line="271"/>
        <source>No data to save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BHCurve.py" line="175"/>
        <location filename="../MPU6050.py" line="221"/>
        <location filename="../RCtransient.py" line="194"/>
        <location filename="../RLCsteadystate.py" line="392"/>
        <location filename="../RLCtransient.py" line="194"/>
        <location filename="../RLtransient.py" line="219"/>
        <location filename="../XYplot.py" line="200"/>
        <location filename="../XYplot2.py" line="202"/>
        <location filename="../diodeIV.py" line="186"/>
        <location filename="../drivenRodPendulum.py" line="271"/>
        <location filename="../filterCircuit.py" line="337"/>
        <location filename="../induction.py" line="168"/>
        <location filename="../lightsensorlogger.py" line="230"/>
        <location filename="../logger.py" line="222"/>
        <location filename="../npnCEout.py" line="185"/>
        <location filename="../pendulumVelocity.py" line="212"/>
        <location filename="../plotIV.py" line="269"/>
        <location filename="../pnpCEout.py" line="183"/>
        <location filename="../pt100.py" line="318"/>
        <location filename="../rodPendulum.py" line="240"/>
        <location filename="../scope.py" line="262"/>
        <location filename="../scope.py" line="497"/>
        <location filename="../soundFreqResp.py" line="288"/>
        <location filename="../sr04dist.py" line="276"/>
        <location filename="../sr04dist_fft.py" line="276"/>
        <location filename="../thermocouplelogger.py" line="228"/>
        <source>Traces saved to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BHCurve.py" line="181"/>
        <location filename="../MPU6050.py" line="227"/>
        <location filename="../RCtransient.py" line="236"/>
        <location filename="../RLCsteadystate.py" line="440"/>
        <location filename="../RLCtransient.py" line="211"/>
        <location filename="../RLtransient.py" line="236"/>
        <location filename="../XYplot.py" line="241"/>
        <location filename="../XYplot2.py" line="243"/>
        <location filename="../diodeIV.py" line="192"/>
        <location filename="../driven-pendulum.py" line="66"/>
        <location filename="../drivenRodPendulum.py" line="277"/>
        <location filename="../dust_sensor.py" line="122"/>
        <location filename="../filterCircuit.py" line="344"/>
        <location filename="../induction.py" line="174"/>
        <location filename="../lightsensorlogger.py" line="236"/>
        <location filename="../logger.py" line="228"/>
        <location filename="../npnCEout.py" line="191"/>
        <location filename="../pendulumVelocity.py" line="218"/>
        <location filename="../plotIV.py" line="275"/>
        <location filename="../pnpCEout.py" line="189"/>
        <location filename="../pt100.py" line="324"/>
        <location filename="../rodPendulum.py" line="246"/>
        <location filename="../scope.py" line="720"/>
        <location filename="../soundBeats.py" line="260"/>
        <location filename="../soundFreqResp.py" line="294"/>
        <location filename="../soundVelocity.py" line="229"/>
        <location filename="../sr04dist.py" line="282"/>
        <location filename="../sr04dist_fft.py" line="282"/>
        <location filename="../thermocouplelogger.py" line="234"/>
        <location filename="../tof.py" line="89"/>
        <source>Error. Try Device-&gt;Reconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../diodeIV.py" line="38"/>
        <location filename="../induction.py" line="49"/>
        <location filename="../npnCEout.py" line="42"/>
        <location filename="../pendulumVelocity.py" line="45"/>
        <location filename="../pnpCEout.py" line="40"/>
        <source>Voltage (V)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../diodeIV.py" line="40"/>
        <location filename="../npnCEout.py" line="44"/>
        <location filename="../pnpCEout.py" line="42"/>
        <source>Current (mA)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../diodeIV.py" line="50"/>
        <source>Zener Diode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../diodeIV.py" line="61"/>
        <source>FIT with I=Io* exp(qV/nkT)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../diodeIV.py" line="109"/>
        <source>Fitted with Diode Equation : Io = </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../diodeIV.py" line="109"/>
        <source> mA , Ideality factor = </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../diodeIV.py" line="112"/>
        <location filename="../drivenRodPendulum.py" line="155"/>
        <location filename="../pendulumVelocity.py" line="144"/>
        <location filename="../rodPendulum.py" line="140"/>
        <source>Analysis failed. Could not fit data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../diodeIV.py" line="133"/>
        <location filename="../npnCEout.py" line="120"/>
        <location filename="../pnpCEout.py" line="117"/>
        <source>Completed plotting I-V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../driven-pendulum.py" line="34"/>
        <source>This program sets SQ1 to high resolution mode. WG will be disabled.
Frequency can be changed from 0.1 Hz to 50Hz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../driven-pendulum.py" line="37"/>
        <location filename="../drivenRodPendulum.py" line="84"/>
        <source>Set SQ1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../driven-pendulum.py" line="58"/>
        <location filename="../drivenRodPendulum.py" line="161"/>
        <location filename="../drivenRodPendulum.py" line="169"/>
        <location filename="../pendulumVelocity.py" line="120"/>
        <location filename="../scope.py" line="608"/>
        <location filename="../tof.py" line="81"/>
        <source>sqr1 set to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RCtransient.py" line="211"/>
        <location filename="../RLCsteadystate.py" line="410"/>
        <location filename="../XYplot.py" line="211"/>
        <location filename="../XYplot2.py" line="213"/>
        <location filename="../driven-pendulum.py" line="58"/>
        <location filename="../drivenRodPendulum.py" line="161"/>
        <location filename="../drivenRodPendulum.py" line="169"/>
        <location filename="../filterCircuit.py" line="233"/>
        <location filename="../pendulumVelocity.py" line="120"/>
        <location filename="../scope.py" line="633"/>
        <location filename="../soundVelocity.py" line="173"/>
        <location filename="../soundVelocity.py" line="207"/>
        <location filename="../tof.py" line="81"/>
        <source> Hz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../drivenRodPendulum.py" line="38"/>
        <location filename="../rodPendulum.py" line="38"/>
        <source>Trials</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../drivenRodPendulum.py" line="40"/>
        <location filename="../rodPendulum.py" line="40"/>
        <source>Time Period (mSec)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../drivenRodPendulum.py" line="57"/>
        <location filename="../rodPendulum.py" line="57"/>
        <source>Pendulum length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../drivenRodPendulum.py" line="62"/>
        <location filename="../rodPendulum.py" line="62"/>
        <location filename="../sr04dist.py" line="57"/>
        <location filename="../sr04dist_fft.py" line="57"/>
        <source>cm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../drivenRodPendulum.py" line="67"/>
        <location filename="../rodPendulum.py" line="67"/>
        <source>Clear Data and Traces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../drivenRodPendulum.py" line="91"/>
        <source>Set WG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../drivenRodPendulum.py" line="99"/>
        <location filename="../rodPendulum.py" line="84"/>
        <source>Number of trials</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../drivenRodPendulum.py" line="153"/>
        <location filename="../pendulumVelocity.py" line="141"/>
        <location filename="../rodPendulum.py" line="138"/>
        <source>Frequency of Oscillation = </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../drivenRodPendulum.py" line="153"/>
        <location filename="../pendulumVelocity.py" line="141"/>
        <location filename="../rodPendulum.py" line="138"/>
        <source> Hz. Damping Factor = </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../drivenRodPendulum.py" line="187"/>
        <location filename="../rodPendulum.py" line="156"/>
        <source>Timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../drivenRodPendulum.py" line="201"/>
        <location filename="../rodPendulum.py" line="170"/>
        <source>Completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../drivenRodPendulum.py" line="217"/>
        <location filename="../rodPendulum.py" line="186"/>
        <source>Invalid Number of trials</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../drivenRodPendulum.py" line="223"/>
        <location filename="../rodPendulum.py" line="192"/>
        <source>Invalid Length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MPU6050.py" line="195"/>
        <location filename="../drivenRodPendulum.py" line="241"/>
        <location filename="../lightsensorlogger.py" line="207"/>
        <location filename="../logger.py" line="199"/>
        <location filename="../pendulumVelocity.py" line="189"/>
        <location filename="../pt100.py" line="294"/>
        <location filename="../rodPendulum.py" line="210"/>
        <location filename="../sr04dist.py" line="250"/>
        <location filename="../sr04dist_fft.py" line="250"/>
        <location filename="../thermocouplelogger.py" line="205"/>
        <source>Started Measurements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dust_sensor.py" line="38"/>
        <source>Time (S)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RCtransient.py" line="61"/>
        <location filename="../RLCsteadystate.py" line="85"/>
        <location filename="../RLCtransient.py" line="60"/>
        <location filename="../RLtransient.py" line="59"/>
        <location filename="../XYplot2.py" line="83"/>
        <location filename="../dust_sensor.py" line="40"/>
        <location filename="../scope.py" line="126"/>
        <location filename="../soundBeats.py" line="62"/>
        <location filename="../soundVelocity.py" line="60"/>
        <source>Voltage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dust_sensor.py" line="50"/>
        <source>Ratio =</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dust_sensor.py" line="52"/>
        <source>Occupancy =</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dust_sensor.py" line="54"/>
        <source>Average Occupancy =</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dust_sensor.py" line="56"/>
        <source>Concentration =</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RCtransient.py" line="121"/>
        <location filename="../RLCsteadystate.py" line="202"/>
        <location filename="../RLCtransient.py" line="120"/>
        <location filename="../RLtransient.py" line="123"/>
        <location filename="../XYplot.py" line="130"/>
        <location filename="../XYplot2.py" line="154"/>
        <location filename="../dust_sensor.py" line="67"/>
        <location filename="../soundBeats.py" line="151"/>
        <location filename="../soundVelocity.py" line="120"/>
        <source>messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../editor.py" line="36"/>
        <source>Execute Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../editor.py" line="39"/>
        <source>Save Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../editor.py" line="67"/>
        <source>Code saved to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filterCircuit.py" line="54"/>
        <location filename="../soundFreqResp.py" line="48"/>
        <source>Frequency (Hz)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>From</source>
        <translation type="obsolete">From</translation>
    </message>
    <message>
        <source>to</source>
        <translation type="obsolete">to</translation>
    </message>
    <message>
        <location filename="../RLCsteadystate.py" line="138"/>
        <location filename="../XYplot.py" line="86"/>
        <location filename="../XYplot2.py" line="113"/>
        <location filename="../filterCircuit.py" line="96"/>
        <location filename="../filterCircuit.py" line="107"/>
        <location filename="../soundBeats.py" line="84"/>
        <location filename="../soundBeats.py" line="97"/>
        <location filename="../soundFreqResp.py" line="66"/>
        <location filename="../soundFreqResp.py" line="77"/>
        <location filename="../soundVelocity.py" line="82"/>
        <location filename="../sr04dist.py" line="134"/>
        <location filename="../sr04dist.py" line="159"/>
        <location filename="../sr04dist_fft.py" line="134"/>
        <location filename="../sr04dist_fft.py" line="159"/>
        <source>Hz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of Steps =</source>
        <translation type="obsolete">Number of Steps =</translation>
    </message>
    <message>
        <location filename="../filterCircuit.py" line="233"/>
        <source>Frequency = </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filterCircuit.py" line="251"/>
        <source>Fit failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filterCircuit.py" line="280"/>
        <location filename="../soundFreqResp.py" line="223"/>
        <source>Invalid Frequency limits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filterCircuit.py" line="315"/>
        <source>user Stopped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filterCircuit.py" line="319"/>
        <location filename="../filterCircuit.py" line="329"/>
        <location filename="../soundFreqResp.py" line="278"/>
        <location filename="../sr04dist.py" line="268"/>
        <location filename="../sr04dist_fft.py" line="268"/>
        <source>Measurement in progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../i2cLogger.py" line="535"/>
        <source>Scan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MPU6050.py" line="53"/>
        <location filename="../RCtransient.py" line="59"/>
        <location filename="../RLCsteadystate.py" line="83"/>
        <location filename="../RLCtransient.py" line="58"/>
        <location filename="../RLtransient.py" line="57"/>
        <location filename="../induction.py" line="47"/>
        <location filename="../lightsensorlogger.py" line="59"/>
        <location filename="../logger.py" line="53"/>
        <location filename="../pendulumVelocity.py" line="43"/>
        <location filename="../pt100.py" line="53"/>
        <location filename="../scope.py" line="123"/>
        <location filename="../soundBeats.py" line="60"/>
        <location filename="../soundVelocity.py" line="58"/>
        <location filename="../thermocouplelogger.py" line="53"/>
        <source>Time (mS)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../induction.py" line="62"/>
        <source>Select Range of A1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../induction.py" line="65"/>
        <source>4 V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../induction.py" line="74"/>
        <source>Start Scanning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../induction.py" line="130"/>
        <source>Noise = </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../induction.py" line="130"/>
        <source> V. Drop the Magnet until a trace is captured</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../induction.py" line="145"/>
        <source>Induced voltage </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../induction.py" line="148"/>
        <source>Detected voltage above threshold. Peak voltages: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lightsensorlogger.py" line="61"/>
        <location filename="../logger.py" line="55"/>
        <source>Voltage(V)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lightsensorlogger.py" line="75"/>
        <location filename="../logger.py" line="69"/>
        <location filename="../pt100.py" line="132"/>
        <location filename="../thermocouplelogger.py" line="69"/>
        <source>Total Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MPU6050.py" line="77"/>
        <location filename="../lightsensorlogger.py" line="80"/>
        <location filename="../lightsensorlogger.py" line="91"/>
        <location filename="../logger.py" line="74"/>
        <location filename="../logger.py" line="85"/>
        <location filename="../pendulumVelocity.py" line="61"/>
        <location filename="../pt100.py" line="137"/>
        <location filename="../pt100.py" line="148"/>
        <location filename="../thermocouplelogger.py" line="74"/>
        <location filename="../thermocouplelogger.py" line="85"/>
        <source>Seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lightsensorlogger.py" line="86"/>
        <location filename="../logger.py" line="80"/>
        <location filename="../pt100.py" line="143"/>
        <location filename="../thermocouplelogger.py" line="80"/>
        <source>Measure every</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lightsensorlogger.py" line="144"/>
        <source>TSL2561 Sensor Not Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lightsensorlogger.py" line="173"/>
        <location filename="../logger.py" line="165"/>
        <location filename="../thermocouplelogger.py" line="170"/>
        <source>Data logger plot completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lightsensorlogger.py" line="182"/>
        <location filename="../logger.py" line="174"/>
        <location filename="../pendulumVelocity.py" line="179"/>
        <location filename="../pt100.py" line="255"/>
        <location filename="../sr04dist.py" line="230"/>
        <location filename="../sr04dist_fft.py" line="230"/>
        <location filename="../thermocouplelogger.py" line="179"/>
        <source>Invalid Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lightsensorlogger.py" line="191"/>
        <location filename="../logger.py" line="183"/>
        <location filename="../pt100.py" line="264"/>
        <location filename="../thermocouplelogger.py" line="189"/>
        <source>Invalid time interval between reads</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MPU6050.py" line="32"/>
        <source>Ax</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MPU6050.py" line="33"/>
        <source>Ay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MPU6050.py" line="34"/>
        <source>Az</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MPU6050.py" line="35"/>
        <source>Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MPU6050.py" line="36"/>
        <source>Vx</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MPU6050.py" line="37"/>
        <source>Vy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MPU6050.py" line="40"/>
        <source>Vz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MPU6050.py" line="55"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MPU6050.py" line="72"/>
        <location filename="../pendulumVelocity.py" line="56"/>
        <source>Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MPU6050.py" line="83"/>
        <source>Read every</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MPU6050.py" line="88"/>
        <source>mS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MPU6050.py" line="137"/>
        <source>I2C device communication error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MPU6050.py" line="155"/>
        <source>MPU6050 data plot completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MPU6050.py" line="170"/>
        <source>Invalid Duration or Time between reads (&gt; 10 mSec)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MPU6050.py" line="212"/>
        <location filename="../soundFreqResp.py" line="282"/>
        <source>No Traces available for saving</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../npnCEout.py" line="55"/>
        <location filename="../pnpCEout.py" line="53"/>
        <source>Vbase (via 100kOhm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../npnCEout.py" line="60"/>
        <location filename="../plotIV.py" line="80"/>
        <location filename="../plotIV.py" line="91"/>
        <location filename="../pnpCEout.py" line="58"/>
        <source>V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../npnCEout.py" line="138"/>
        <source>Base voltage should be from .5 to 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../npnCEout.py" line="140"/>
        <source>Invalid Base voltage, should be from .5 to 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pendulumVelocity.py" line="74"/>
        <location filename="../plotIV.py" line="105"/>
        <source>Analyze last Trace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pendulumVelocity.py" line="87"/>
        <source>
Set SQ1 Frequency
For Driven Pendulum Expt.</source>
        <translation type="unfinished">
Set SQ1 Frequency
For Driven Pendulum Expt.</translation>
    </message>
    <message>
        <location filename="../pendulumVelocity.py" line="167"/>
        <source>Time Vs Angular velocity plot completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plotIV.py" line="48"/>
        <source>Current through R (mA)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plotIV.py" line="50"/>
        <source>Voltage across R(Volts)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plotIV.py" line="63"/>
        <source>R to Ground</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RCtransient.py" line="103"/>
        <location filename="../RLCtransient.py" line="95"/>
        <location filename="../RLtransient.py" line="102"/>
        <location filename="../plotIV.py" line="68"/>
        <source>Ohm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plotIV.py" line="75"/>
        <source>Starting PV1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plotIV.py" line="117"/>
        <source>Change Voltage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plotIV.py" line="124"/>
        <source>Voltage = %5.3f</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plotIV.py" line="129"/>
        <source>Current = 0 mA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plotIV.py" line="161"/>
        <source>Voltage = %5.3f V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plotIV.py" line="163"/>
        <source>Current = %5.3f mA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plotIV.py" line="176"/>
        <source>Slope of the Line (dV/dI) = </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plotIV.py" line="228"/>
        <source>Err</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pnpCEout.py" line="136"/>
        <source>Base voltage should be from -0.5 to -3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pnpCEout.py" line="138"/>
        <source>Invalid Base voltage, should be from -0.5 to -3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pt100.py" line="55"/>
        <source>Temperature (C)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pt100.py" line="66"/>
        <source>Measure A3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pt100.py" line="76"/>
        <source>A3 Gain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pt100.py" line="81"/>
        <source>1+10k/Rg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pt100.py" line="87"/>
        <source>A3 Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pt100.py" line="92"/>
        <source>mV </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pt100.py" line="98"/>
        <source>CCS Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pt100.py" line="103"/>
        <source>mA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pt100.py" line="110"/>
        <source>Lowest Temp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pt100.py" line="115"/>
        <location filename="../pt100.py" line="126"/>
        <source>deg C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pt100.py" line="121"/>
        <source>Highest Temp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pt100.py" line="210"/>
        <source>%5.3f V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pt100.py" line="242"/>
        <source>Time Vs Temperature plot completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pt100.py" line="271"/>
        <source>Invalid temperature limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pt100.py" line="278"/>
        <source>Invalid Offset or Gain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pt100.py" line="284"/>
        <source>Invalid CCS input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RCtransient.py" line="71"/>
        <location filename="../RLCsteadystate.py" line="123"/>
        <location filename="../RLCtransient.py" line="70"/>
        <location filename="../RLtransient.py" line="78"/>
        <location filename="../soundBeats.py" line="119"/>
        <location filename="../soundVelocity.py" line="88"/>
        <source>Timebase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RCtransient.py" line="76"/>
        <location filename="../RLCtransient.py" line="75"/>
        <location filename="../RLtransient.py" line="83"/>
        <location filename="../soundBeats.py" line="124"/>
        <location filename="../soundVelocity.py" line="93"/>
        <source>mS/div</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RCtransient.py" line="81"/>
        <location filename="../RLCtransient.py" line="81"/>
        <location filename="../RLtransient.py" line="88"/>
        <source>0 -&gt; 5V step on OD1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RCtransient.py" line="85"/>
        <location filename="../RLCtransient.py" line="85"/>
        <location filename="../RLtransient.py" line="92"/>
        <source>5 -&gt; 0V step on OD1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RCtransient.py" line="89"/>
        <source>Calculate RC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RCtransient.py" line="93"/>
        <location filename="../RLtransient.py" line="111"/>
        <source>Clear Data &amp; Traces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RCtransient.py" line="98"/>
        <source>Resistance =</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RCtransient.py" line="137"/>
        <location filename="../RLCtransient.py" line="136"/>
        <location filename="../RLtransient.py" line="159"/>
        <location filename="../sr04dist.py" line="128"/>
        <location filename="../sr04dist.py" line="150"/>
        <location filename="../sr04dist_fft.py" line="128"/>
        <location filename="../sr04dist_fft.py" line="150"/>
        <source>No data to analyze.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RCtransient.py" line="150"/>
        <source>Fitted data with V=Vo*exp(-t/RC). RC = </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RCtransient.py" line="150"/>
        <source> mSec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RCtransient.py" line="152"/>
        <source>Failed to fit the curve with V=Vo*exp(-t/RC)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RCtransient.py" line="185"/>
        <source>Cleared Data and Traces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RCtransient.py" line="211"/>
        <location filename="../RLCsteadystate.py" line="410"/>
        <location filename="../XYplot.py" line="211"/>
        <location filename="../XYplot2.py" line="213"/>
        <location filename="../scope.py" line="633"/>
        <location filename="../soundVelocity.py" line="207"/>
        <source>AWG set to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLCsteadystate.py" line="131"/>
        <location filename="../XYplot.py" line="79"/>
        <location filename="../XYplot2.py" line="106"/>
        <location filename="../soundBeats.py" line="77"/>
        <location filename="../soundVelocity.py" line="75"/>
        <source>WG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLCsteadystate.py" line="156"/>
        <source>Impedance Calculator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLCsteadystate.py" line="160"/>
        <source>F (in Hz)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLCsteadystate.py" line="166"/>
        <source>R (in Ohms)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLCsteadystate.py" line="175"/>
        <source>C (in uF)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLCsteadystate.py" line="180"/>
        <source>L (in mH)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLCsteadystate.py" line="188"/>
        <source>Calculate XL, XC and Fo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLCsteadystate.py" line="219"/>
        <source>Curve fitting result rejected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLCsteadystate.py" line="262"/>
        <location filename="../RLCsteadystate.py" line="283"/>
        <source>Data Analysis Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLCsteadystate.py" line="288"/>
        <source>Vtotal (A1 = %5.2f V)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLCsteadystate.py" line="289"/>
        <source>Vr (A2 = %5.2f V)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLCsteadystate.py" line="290"/>
        <source>Vlc (A2-A1 = %5.2f V)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLCsteadystate.py" line="292"/>
        <source>F = %5.1f Hz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLCsteadystate.py" line="293"/>
        <source>Phase Diff = %5.1f deg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLCsteadystate.py" line="296"/>
        <source>Vc (A3-A1 = %5.2f V)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLCsteadystate.py" line="297"/>
        <source>Vl (A2-A3 = %5.2f V)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLCsteadystate.py" line="378"/>
        <source>Invalid Input in some field</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLCtransient.py" line="90"/>
        <location filename="../RLtransient.py" line="97"/>
        <source>Rext =</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLCtransient.py" line="100"/>
        <source>Analyse latest Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLCtransient.py" line="108"/>
        <source>Clear Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLCtransient.py" line="145"/>
        <source>Resonant Frequency = </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLCtransient.py" line="145"/>
        <source> kHz Damping factor= </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLCtransient.py" line="150"/>
        <location filename="../sr04dist.py" line="138"/>
        <location filename="../sr04dist.py" line="178"/>
        <location filename="../sr04dist_fft.py" line="138"/>
        <location filename="../sr04dist_fft.py" line="178"/>
        <source>Failed to fit the curve</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLtransient.py" line="69"/>
        <location filename="../XYplot2.py" line="97"/>
        <source>Save Data to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLtransient.py" line="73"/>
        <source>RCtransient.txt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLtransient.py" line="107"/>
        <source>Analyse last Trace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLtransient.py" line="151"/>
        <source>Enter a valid Resistance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLtransient.py" line="181"/>
        <source>L/R = </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLtransient.py" line="181"/>
        <source> mSec : Rind = </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLtransient.py" line="181"/>
        <source> Ohm : L = </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLtransient.py" line="181"/>
        <source> mH</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../RLtransient.py" line="184"/>
        <source>Failed to fit the curve with V=Vo*exp(-t*L/R)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../scope.py" line="168"/>
        <source>Reconnecting...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../scope.py" line="210"/>
        <source>Error. Could not connect. Check cable. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../scope.py" line="235"/>
        <location filename="../scope.py" line="470"/>
        <location filename="../sr04dist.py" line="171"/>
        <location filename="../sr04dist_fft.py" line="171"/>
        <source>Fundamental frequency = %5.1f Hz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../scope.py" line="238"/>
        <location filename="../scope.py" line="473"/>
        <location filename="../soundBeats.py" line="133"/>
        <location filename="../soundBeats.py" line="166"/>
        <location filename="../sr04dist.py" line="174"/>
        <location filename="../sr04dist_fft.py" line="174"/>
        <source>Frequency Spectrum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../scope.py" line="240"/>
        <location filename="../scope.py" line="475"/>
        <source>FFT Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../scope.py" line="283"/>
        <source>Time: %6.2fmS </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../scope.py" line="291"/>
        <source>%s:%6.2fV </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../scope.py" line="347"/>
        <source>%s input is clipped. Increase range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../scope.py" line="360"/>
        <source>%5.2f V, %5.1f Hz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../scope.py" line="390"/>
        <source>A%d %5.3f V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../scope.py" line="392"/>
        <source>A%d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../scope.py" line="397"/>
        <source>%5.0f Ohm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../scope.py" line="399"/>
        <source>Resistance: &lt;100Ohm  or  &gt;100k</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../scope.py" line="445"/>
        <source>Range of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../scope.py" line="445"/>
        <source> set to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../scope.py" line="636"/>
        <source>Output Changed from WG to SQ2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../scope.py" line="686"/>
        <source>Capacitance too high or short to ground</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../scope.py" line="690"/>
        <source> pF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../scope.py" line="693"/>
        <source> nF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../scope.py" line="696"/>
        <source> uF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../scope.py" line="709"/>
        <source>%5.1fHz %4.1f%%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../scope.py" line="711"/>
        <source>No signal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../soundBeats.py" line="90"/>
        <source>SQ1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change of Freqency effected
only after Enable/Disable controls.
shows the actual frequency set.
</source>
        <translation type="obsolete">Change of Freqency effected
only after Enable/Disable controls.
shows the actual frequency set.
</translation>
    </message>
    <message>
        <location filename="../soundBeats.py" line="103"/>
        <source>Change of Freqency effected
only after Enable/Disable controls.

shows the actual frequency set.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../soundBeats.py" line="106"/>
        <source>Enable WG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../soundBeats.py" line="110"/>
        <source>Enable SQ1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../soundBeats.py" line="114"/>
        <location filename="../soundVelocity.py" line="98"/>
        <source>Enable Measurements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../soundBeats.py" line="171"/>
        <source>FFT err</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../soundBeats.py" line="205"/>
        <location filename="../soundVelocity.py" line="178"/>
        <source>Disable before Saving</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../soundBeats.py" line="217"/>
        <location filename="../soundVelocity.py" line="190"/>
        <source>Trace saved to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../soundFreqResp.py" line="50"/>
        <source>Amplitude (V)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>in</source>
        <translation type="obsolete">in</translation>
    </message>
    <message>
        <source>seconds</source>
        <translation type="obsolete">seconds</translation>
    </message>
    <message>
        <location filename="../soundFreqResp.py" line="183"/>
        <source>Frequency = %5.0f Hz V = %5.3f</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../soundFreqResp.py" line="202"/>
        <source>Completed in </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../soundFreqResp.py" line="202"/>
        <location filename="../tof.py" line="73"/>
        <source> Seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../soundFreqResp.py" line="229"/>
        <source>Invalid Time interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../soundFreqResp.py" line="242"/>
        <source>Increase time interval to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../soundFreqResp.py" line="242"/>
        <source> or Reduce frequency span</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../soundFreqResp.py" line="254"/>
        <source> mS at each step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../soundVelocity.py" line="173"/>
        <source>Phase Shift = </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sr04dist.py" line="39"/>
        <location filename="../sr04dist_fft.py" line="39"/>
        <source>Time (Sec)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sr04dist.py" line="41"/>
        <location filename="../sr04dist_fft.py" line="41"/>
        <source>Distance(cm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sr04dist.py" line="52"/>
        <location filename="../sr04dist_fft.py" line="52"/>
        <source>Y-axis from 0 to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sr04dist.py" line="64"/>
        <location filename="../sr04dist_fft.py" line="64"/>
        <source>Measure during</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sr04dist.py" line="69"/>
        <location filename="../sr04dist_fft.py" line="69"/>
        <source>Secs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sr04dist.py" line="83"/>
        <location filename="../sr04dist_fft.py" line="83"/>
        <source>Fit Curve using Sine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sr04dist.py" line="90"/>
        <location filename="../sr04dist_fft.py" line="90"/>
        <source>Fourier Transform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sr04dist.py" line="134"/>
        <location filename="../sr04dist.py" line="159"/>
        <location filename="../sr04dist_fft.py" line="134"/>
        <location filename="../sr04dist_fft.py" line="159"/>
        <source>Sine Fit Result: Frequency </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sr04dist.py" line="215"/>
        <location filename="../sr04dist_fft.py" line="215"/>
        <source>Time vs Distance plot completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tof.py" line="41"/>
        <source>Measure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tof.py" line="67"/>
        <source>start..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tof.py" line="73"/>
        <source>Time of flight =</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tof.py" line="75"/>
        <source>Error. Try again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../XYplot.py" line="56"/>
        <location filename="../XYplot2.py" line="54"/>
        <source>Voltage  A2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../XYplot.py" line="147"/>
        <location filename="../XYplot2.py" line="56"/>
        <location filename="../XYplot2.py" line="71"/>
        <location filename="../XYplot2.py" line="171"/>
        <source>Voltage (A2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../XYplot2.py" line="69"/>
        <source>Voltage  A1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../XYplot2.py" line="81"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../XYplot2.py" line="101"/>
        <source>XYplot.txt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../XYplot.py" line="92"/>
        <location filename="../XYplot2.py" line="119"/>
        <source>Voltage range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../XYplot.py" line="97"/>
        <location filename="../XYplot2.py" line="124"/>
        <source>Volts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../XYplot.py" line="102"/>
        <location filename="../XYplot2.py" line="129"/>
        <source>show (A1-A2) Vs A2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../XYplot.py" line="145"/>
        <location filename="../XYplot2.py" line="169"/>
        <source>Voltage (A1-A2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../XYplot.py" line="184"/>
        <source>Xmax = %5.3f V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../XYplot.py" line="185"/>
        <source>Ymax = %5.3f V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../XYplot.py" line="58"/>
        <source>Voltage (A1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../XYplot.py" line="164"/>
        <source>Y-intercept = %5.3f V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plotIV.py" line="86"/>
        <source>Ending PV1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filterCircuit.py" line="91"/>
        <location filename="../soundFreqResp.py" line="61"/>
        <source>Starting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filterCircuit.py" line="102"/>
        <location filename="../soundFreqResp.py" line="72"/>
        <source>Ending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../soundFreqResp.py" line="83"/>
        <source>Total time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../soundFreqResp.py" line="88"/>
        <source>Sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../scope.py" line="186"/>
        <source>Calibrated </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../scope.py" line="188"/>
        <source>Not Calibrated </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../scope.py" line="188"/>
        <source>Device Reconnected:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filterCircuit.py" line="56"/>
        <source>Amplitude</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filterCircuit.py" line="67"/>
        <source>WG range </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filterCircuit.py" line="79"/>
        <source>A2 range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filterCircuit.py" line="113"/>
        <source>Steps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filterCircuit.py" line="212"/>
        <source>Peak = %5.3f V at %4.1f Hz.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filterCircuit.py" line="297"/>
        <source>Starting. Input Vp = %4.2f Volts at 1kHz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filterCircuit.py" line="299"/>
        <source>fit err.No proper input on A1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../thermocouplelogger.py" line="55"/>
        <source>Temperature(C)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../thermocouplelogger.py" line="156"/>
        <source>thermocouple not attached. : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../thermocouplelogger.py" line="159"/>
        <source>Temp: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../blockcoding.py" line="440"/>
        <source>Blockly is missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../blockcoding.py" line="444"/>
        <source>
You wanted to launch eyes17&apos;s blockly plugin.
Unfortunately the plugin is missing... Consider
installing it (it is a non-free package).</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../layouts/blockly_layout.ui" line="14"/>
        <location filename="../layouts/browser_layout.ui" line="14"/>
        <location filename="../layouts/list_layout.ui" line="14"/>
        <location filename="../layouts/miniScope.ui" line="14"/>
        <location filename="../layouts/scope_layout.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/miniScope.ui" line="138"/>
        <source>Channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/miniScope.ui" line="182"/>
        <source>1mS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/miniScope.ui" line="194"/>
        <location filename="../layouts/scope_layout.ui" line="506"/>
        <location filename="../layouts/scope_layout.ui" line="1297"/>
        <source>A2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/miniScope.ui" line="207"/>
        <source>Results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/miniScope.ui" line="228"/>
        <source>Cross Check Freq</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="138"/>
        <source>A2-A1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="1323"/>
        <source>.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="251"/>
        <source>Measure Voltages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="297"/>
        <location filename="../layouts/scope_layout.ui" line="433"/>
        <source>16V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="302"/>
        <location filename="../layouts/scope_layout.ui" line="438"/>
        <source>8V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="307"/>
        <location filename="../layouts/scope_layout.ui" line="443"/>
        <location filename="../layouts/scope_layout.ui" line="528"/>
        <location filename="../layouts/scope_layout.ui" line="580"/>
        <source>4V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="312"/>
        <location filename="../layouts/scope_layout.ui" line="448"/>
        <source>2.5V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="317"/>
        <location filename="../layouts/scope_layout.ui" line="453"/>
        <location filename="../layouts/scope_layout.ui" line="538"/>
        <location filename="../layouts/scope_layout.ui" line="590"/>
        <location filename="../layouts/scope_layout.ui" line="1204"/>
        <source>1V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="322"/>
        <location filename="../layouts/scope_layout.ui" line="458"/>
        <location filename="../layouts/scope_layout.ui" line="543"/>
        <location filename="../layouts/scope_layout.ui" line="595"/>
        <source>0.5V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="347"/>
        <location filename="../layouts/scope_layout.ui" line="374"/>
        <location filename="../layouts/scope_layout.ui" line="401"/>
        <location filename="../layouts/scope_layout.ui" line="496"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Amplitude&lt;/span&gt; and &lt;span style=&quot; font-weight:600;&quot;&gt;frequency&lt;/span&gt; extracted from the&lt;/p&gt;&lt;p&gt;sinusoidal signal using least square fitting.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;This assumes a sine wave input is provided.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;For mixed signals, use the FOURIER transform button&lt;/p&gt;&lt;p&gt;below.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="551"/>
        <location filename="../layouts/scope_layout.ui" line="1309"/>
        <source>A3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="533"/>
        <location filename="../layouts/scope_layout.ui" line="585"/>
        <source>2V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="411"/>
        <source>MIC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="776"/>
        <location filename="../layouts/scope_layout.ui" line="854"/>
        <source> Volts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="897"/>
        <source> %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="916"/>
        <location filename="../layouts/scope_layout.ui" line="959"/>
        <source>symmetric</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="996"/>
        <location filename="../layouts/scope_layout.ui" line="1015"/>
        <source> Hz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="710"/>
        <location filename="../layouts/scope_layout.ui" line="822"/>
        <location filename="../layouts/scope_layout.ui" line="841"/>
        <source>symmetric volts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="884"/>
        <source>SQ1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="940"/>
        <source>PV2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="983"/>
        <source>WG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="806"/>
        <source>PV1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Current Source Voltage:</source>
        <translation type="obsolete">Current Source Voltage:</translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="742"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The voltage value at the constant current source output&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="1049"/>
        <source>Timebase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="1113"/>
        <source>Trigger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="1126"/>
        <source>SAVE Traces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="1133"/>
        <source>CCS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="1147"/>
        <source>Fourier Transform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="1167"/>
        <source>Resistance on SEN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="1174"/>
        <source>Frequency (IN2):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="1181"/>
        <source>OD1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="1199"/>
        <source>80 mV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="1209"/>
        <source>3V (Amplitude)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="1218"/>
        <source>WG( Sinusoidal )</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="1223"/>
        <source>WG( Triangle )</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="1228"/>
        <source>SQ2( Square )</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="1236"/>
        <source>Capacitance (IN1):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="1243"/>
        <source>SHOW ALL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="1285"/>
        <source>A1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="151"/>
        <source>FREEZE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="164"/>
        <source>Cursor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="666"/>
        <source>1.65mA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="685"/>
        <source>PCS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/scope_layout.ui" line="723"/>
        <source>Voltage across PCS(mV):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/browser_layout.ui" line="99"/>
        <source>deep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/browser_layout.ui" line="123"/>
        <source>Double click to launch any experiment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/browser_layout.ui" line="167"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/blockly_layout.ui" line="81"/>
        <source>:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/blockly_layout.ui" line="59"/>
        <source>Examples</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/blockly_layout.ui" line="235"/>
        <location filename="../layouts/list_layout.ui" line="89"/>
        <source>&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../experiment-list.py" line="60"/>
        <location filename="../main.py" line="165"/>
        <source>Voltage measurement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="61"/>
        <location filename="../main.py" line="166"/>
        <source>Resistance measurement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="62"/>
        <location filename="../main.py" line="167"/>
        <source>Resistors in Series</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="63"/>
        <location filename="../main.py" line="168"/>
        <source>Resistors in Parallel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="64"/>
        <location filename="../main.py" line="169"/>
        <source>Capacitance measurement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="65"/>
        <location filename="../main.py" line="170"/>
        <source>Capacitors in Series</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="66"/>
        <location filename="../main.py" line="171"/>
        <source>Capacitors in Parallel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="67"/>
        <location filename="../main.py" line="172"/>
        <source>Resistance by Ohm&apos;s law</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="68"/>
        <location filename="../main.py" line="173"/>
        <source>Direct and Alternating Currents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="69"/>
        <location filename="../main.py" line="174"/>
        <source>AC mains pickup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="70"/>
        <location filename="../main.py" line="175"/>
        <source>Separating AC and DC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="71"/>
        <location filename="../main.py" line="176"/>
        <source>Conducting Human body</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="72"/>
        <location filename="../main.py" line="177"/>
        <source>Resistance of Human body</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="73"/>
        <location filename="../main.py" line="178"/>
        <source>Light Dependent Resistor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="74"/>
        <location filename="../main.py" line="179"/>
        <source>Lemon Cell</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="75"/>
        <location filename="../main.py" line="180"/>
        <source>Simple AC generator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="76"/>
        <location filename="../main.py" line="181"/>
        <source>Transformer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="77"/>
        <location filename="../main.py" line="182"/>
        <source>Resistance of Water</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="78"/>
        <location filename="../main.py" line="183"/>
        <source>Generating Sound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="79"/>
        <location filename="../main.py" line="184"/>
        <source>Digitizing Sound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="80"/>
        <location filename="../main.py" line="185"/>
        <source>Stroboscope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="92"/>
        <location filename="../main.py" line="197"/>
        <source>Oscilloscope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/advancedLogger.ui" line="14"/>
        <location filename="../layouts/newtonslaws.ui" line="14"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/newtonslaws.ui" line="28"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/newtonslaws.ui" line="33"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/newtonslaws.ui" line="38"/>
        <source>3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/newtonslaws.ui" line="43"/>
        <source>4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/newtonslaws.ui" line="48"/>
        <source>5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/newtonslaws.ui" line="53"/>
        <source>6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/newtonslaws.ui" line="58"/>
        <source>7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/newtonslaws.ui" line="63"/>
        <source>8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/newtonslaws.ui" line="68"/>
        <source>9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/newtonslaws.ui" line="73"/>
        <source>10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/newtonslaws.ui" line="81"/>
        <source>RECORD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/newtonslaws.ui" line="91"/>
        <source>Calculate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/newtonslaws.ui" line="111"/>
        <source>Averaging Samples</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/newtonslaws.ui" line="118"/>
        <source>Number of samples considered for moving average in results calculation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/newtonslaws.ui" line="125"/>
        <source>Polynomial Degree</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/newtonslaws.ui" line="132"/>
        <source>Degree of the polynomial used for smoothing the data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/newtonslaws.ui" line="204"/>
        <source>Spline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/advancedLogger.ui" line="42"/>
        <source>CHOOSE Y PARAMETER</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/advancedLogger.ui" line="49"/>
        <source>Click the Button above to select X axis parameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/advancedLogger.ui" line="56"/>
        <source>Click the Button above to select Y axis parameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/advancedLogger.ui" line="69"/>
        <source>CHOOSE X PARAMETER</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/advancedLogger.ui" line="107"/>
        <source>Datapoints</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/advancedLogger.ui" line="114"/>
        <location filename="../layouts/advancedLogger.ui" line="154"/>
        <source> milliSeconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/advancedLogger.ui" line="127"/>
        <source>LOG DATA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/advancedLogger.ui" line="134"/>
        <source>Time Interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../layouts/advancedLogger.ui" line="167"/>
        <source>Settling Time b/w X and Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="37"/>
        <location filename="../main.py" line="142"/>
        <source>Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="38"/>
        <location filename="../main.py" line="143"/>
        <source>School Expts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="39"/>
        <location filename="../main.py" line="144"/>
        <source>Electronics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="40"/>
        <location filename="../main.py" line="145"/>
        <source>Electrical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="41"/>
        <location filename="../main.py" line="146"/>
        <source>Sound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="42"/>
        <location filename="../main.py" line="147"/>
        <source>Mechanics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="43"/>
        <location filename="../main.py" line="148"/>
        <source>Other Expts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="44"/>
        <location filename="../main.py" line="149"/>
        <source>I2C Modules</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="46"/>
        <location filename="../main.py" line="151"/>
        <source>PythonCode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="50"/>
        <location filename="../main.py" line="155"/>
        <source>Reconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="51"/>
        <location filename="../main.py" line="156"/>
        <source>LightBackGround</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="52"/>
        <location filename="../main.py" line="157"/>
        <source>DarkBackGround</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="53"/>
        <location filename="../main.py" line="158"/>
        <source>Choose Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="54"/>
        <location filename="../main.py" line="159"/>
        <source>Screenshot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="55"/>
        <location filename="../main.py" line="160"/>
        <source>Whole Window Alt-s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="56"/>
        <location filename="../main.py" line="161"/>
        <source>Graph Only Alt-p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="57"/>
        <location filename="../main.py" line="162"/>
        <source>Credits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="59"/>
        <location filename="../main.py" line="164"/>
        <source>Experiment List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="93"/>
        <location filename="../main.py" line="198"/>
        <source>Halfwave Rectifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="94"/>
        <location filename="../experiment-list.py" line="168"/>
        <location filename="../main.py" line="199"/>
        <location filename="../main.py" line="273"/>
        <source>Fullwave Rectifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="95"/>
        <location filename="../main.py" line="200"/>
        <source>Diode Clipping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="96"/>
        <location filename="../main.py" line="201"/>
        <source>Diode Clamping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="97"/>
        <location filename="../main.py" line="202"/>
        <source>IC555 Multivibrator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="98"/>
        <location filename="../main.py" line="203"/>
        <source>Transistor Amplifier (CE)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="99"/>
        <location filename="../main.py" line="204"/>
        <source>Inverting Amplifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="100"/>
        <location filename="../main.py" line="205"/>
        <source>Non-Inverting Amplifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="101"/>
        <location filename="../main.py" line="206"/>
        <source>Summing Amplifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="102"/>
        <location filename="../main.py" line="207"/>
        <source>Logic Gates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="103"/>
        <location filename="../main.py" line="208"/>
        <source>Clock Divider Circuit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="107"/>
        <location filename="../main.py" line="212"/>
        <source>Diode Characteristics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="108"/>
        <location filename="../main.py" line="213"/>
        <source>NPN Output Characteristics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="109"/>
        <location filename="../main.py" line="214"/>
        <source>PNP Output Characteristics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="115"/>
        <location filename="../main.py" line="220"/>
        <source>Plot I-V Curve</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="116"/>
        <location filename="../main.py" line="221"/>
        <source>XY Plotting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="117"/>
        <location filename="../main.py" line="222"/>
        <source>RLC Steady state response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="118"/>
        <location filename="../main.py" line="223"/>
        <source>RC Transient response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="119"/>
        <location filename="../main.py" line="224"/>
        <source>RL Transient response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="120"/>
        <location filename="../main.py" line="225"/>
        <source>RLC transient response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="121"/>
        <location filename="../main.py" line="226"/>
        <source>Frequency Response of Filter Circuit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="122"/>
        <location filename="../main.py" line="227"/>
        <source>Electromagnetic Induction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="126"/>
        <location filename="../main.py" line="231"/>
        <source>Frequency Response of Piezo Buzzer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="127"/>
        <location filename="../main.py" line="232"/>
        <source>Velocity of Sound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="128"/>
        <location filename="../main.py" line="233"/>
        <source>Sound beats</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="132"/>
        <location filename="../main.py" line="237"/>
        <source>Rod Pendulum with Light barrier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="133"/>
        <location filename="../main.py" line="238"/>
        <source>Pendulum Waveform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="134"/>
        <location filename="../main.py" line="239"/>
        <source>Driven Pendulum resonance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="135"/>
        <location filename="../main.py" line="240"/>
        <source>Distance by HY-SRF04 Echo module</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="136"/>
        <location filename="../main.py" line="241"/>
        <source>Gravity by Time of Flight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="140"/>
        <location filename="../main.py" line="245"/>
        <source>Temperatue, PT100 Sensor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="141"/>
        <location filename="../main.py" line="246"/>
        <source>Data Logger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="142"/>
        <location filename="../main.py" line="247"/>
        <source>Advanced Data Logger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="143"/>
        <location filename="../main.py" line="248"/>
        <source>Visual Programming Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="147"/>
        <location filename="../main.py" line="252"/>
        <source>Magnetic Hysteresis (MPU925x Sensor)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="148"/>
        <location filename="../main.py" line="253"/>
        <source>Luminosity(TSL2561) Logger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="149"/>
        <location filename="../main.py" line="254"/>
        <source>Temperature(MAX6675) Logger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="150"/>
        <location filename="../main.py" line="255"/>
        <source>MPU-6050 Acccn, Velocity and Temp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="151"/>
        <location filename="../main.py" line="256"/>
        <source>General Purpose I2C Sensors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="155"/>
        <location filename="../main.py" line="260"/>
        <source>Read Inputs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="156"/>
        <location filename="../main.py" line="261"/>
        <source>Set DC Voltages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="157"/>
        <location filename="../main.py" line="262"/>
        <source>Capture Single Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="158"/>
        <location filename="../main.py" line="263"/>
        <source>Capture Two Inputs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="159"/>
        <location filename="../main.py" line="264"/>
        <source>Capture Four Inputs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="160"/>
        <location filename="../main.py" line="265"/>
        <source>Triangular Waveform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="161"/>
        <location filename="../main.py" line="266"/>
        <source>Arbitrary Waveform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="162"/>
        <location filename="../main.py" line="267"/>
        <source>Waveform Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="163"/>
        <location filename="../main.py" line="268"/>
        <source>RC Transient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="164"/>
        <location filename="../main.py" line="269"/>
        <source>RL Transient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="165"/>
        <location filename="../main.py" line="270"/>
        <source>RC Integration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="166"/>
        <location filename="../main.py" line="271"/>
        <source>Clipping with Diode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="167"/>
        <location filename="../main.py" line="272"/>
        <source>Clamping with Diode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="169"/>
        <location filename="../main.py" line="274"/>
        <source>NPN Ib vs IC plot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="170"/>
        <location filename="../main.py" line="275"/>
        <source>Fourier Transform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../experiment-list.py" line="171"/>
        <location filename="../main.py" line="276"/>
        <source>Rod Pendulum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="422"/>
        <source>Enable PopUp Help Window</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>editorHandler</name>
    <message>
        <location filename="../main.py" line="547"/>
        <source>Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="548"/>
        <source>Device Available. Connect?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>helpWin</name>
    <message>
        <location filename="../main.py" line="313"/>
        <source>Help: %s</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>portSelectionDialog</name>
    <message>
        <location filename="../main.py" line="617"/>
        <location filename="../main.py" line="776"/>
        <source>Credits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="657"/>
        <source>Failed to load scope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="699"/>
        <source>Failed to load %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="746"/>
        <source>Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="747"/>
        <source>Reconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="748"/>
        <source>LightBackGround</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="749"/>
        <source>DarkBackGround</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="750"/>
        <source>Choose Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="770"/>
        <source>Language = {} : done = {}, to finish = {}, untranslated = {}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="773"/>
        <source>Screenshot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="774"/>
        <source>Whole Window Alt-s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="775"/>
        <source>Graph Only Alt-p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="777"/>
        <source>Experiment List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="781"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="783"/>
        <source>School Expts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="787"/>
        <source>Electronics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="794"/>
        <source>Electrical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="798"/>
        <source>Sound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="802"/>
        <source>Mechanics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="806"/>
        <source>Other Expts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="810"/>
        <source>I2C Modules</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="814"/>
        <source>PythonCode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="950"/>
        <source>Translating ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="957"/>
        <source>Export screenshot in {name} ({localname}).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="1036"/>
        <source>Missing Plot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="1036"/>
        <source>Unable to locate a plot. Please try to right click and export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="1070"/>
        <source>Enter Width(px). Height will be autoset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.py" line="1120"/>
        <source>Oscilloscope</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>webWin</name>
    <message>
        <location filename="../blockcoding.py" line="94"/>
        <source>Block Coding: %s</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
