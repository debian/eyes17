# Form implementation generated from reading ui file 'screenshot.ui'
#
# Created by: PyQt6 UI code generator 6.4.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic6 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt6 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(680, 543)
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.imgFrame = QtWidgets.QFrame(parent=Dialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Preferred, QtWidgets.QSizePolicy.Policy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(3)
        sizePolicy.setHeightForWidth(self.imgFrame.sizePolicy().hasHeightForWidth())
        self.imgFrame.setSizePolicy(sizePolicy)
        self.imgFrame.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.imgFrame.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.imgFrame.setObjectName("imgFrame")
        self.verticalLayout.addWidget(self.imgFrame)
        self.frame = QtWidgets.QFrame(parent=Dialog)
        self.frame.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.frame.setObjectName("frame")
        self.gridLayout = QtWidgets.QGridLayout(self.frame)
        self.gridLayout.setObjectName("gridLayout")
        self.pathEdit = QtWidgets.QLineEdit(parent=self.frame)
        self.pathEdit.setObjectName("pathEdit")
        self.gridLayout.addWidget(self.pathEdit, 0, 2, 1, 1)
        self.translationLabel = QtWidgets.QLabel(parent=self.frame)
        self.translationLabel.setAlignment(QtCore.Qt.AlignmentFlag.AlignRight|QtCore.Qt.AlignmentFlag.AlignTrailing|QtCore.Qt.AlignmentFlag.AlignVCenter)
        self.translationLabel.setObjectName("translationLabel")
        self.gridLayout.addWidget(self.translationLabel, 1, 0, 1, 1)
        self.langEdit = QtWidgets.QLineEdit(parent=self.frame)
        self.langEdit.setObjectName("langEdit")
        self.gridLayout.addWidget(self.langEdit, 1, 2, 1, 1)
        self.pathLabel = QtWidgets.QLabel(parent=self.frame)
        self.pathLabel.setAlignment(QtCore.Qt.AlignmentFlag.AlignRight|QtCore.Qt.AlignmentFlag.AlignTrailing|QtCore.Qt.AlignmentFlag.AlignVCenter)
        self.pathLabel.setObjectName("pathLabel")
        self.gridLayout.addWidget(self.pathLabel, 0, 0, 1, 1)
        self.widthCheckBox = QtWidgets.QCheckBox(parent=self.frame)
        self.widthCheckBox.setChecked(True)
        self.widthCheckBox.setObjectName("widthCheckBox")
        self.gridLayout.addWidget(self.widthCheckBox, 2, 0, 1, 1)
        self.widthSpinBox = QtWidgets.QSpinBox(parent=self.frame)
        self.widthSpinBox.setMaximum(1000)
        self.widthSpinBox.setSingleStep(10)
        self.widthSpinBox.setProperty("value", 400)
        self.widthSpinBox.setObjectName("widthSpinBox")
        self.gridLayout.addWidget(self.widthSpinBox, 2, 2, 1, 1)
        self.verticalLayout.addWidget(self.frame)
        self.buttonBox = QtWidgets.QDialogButtonBox(parent=Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Orientation.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.StandardButton.Cancel|QtWidgets.QDialogButtonBox.StandardButton.Help|QtWidgets.QDialogButtonBox.StandardButton.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept) # type: ignore
        self.buttonBox.rejected.connect(Dialog.reject) # type: ignore
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Screnshot"))
        self.translationLabel.setText(_translate("Dialog", "Translations ="))
        self.pathLabel.setText(_translate("Dialog", "Path ="))
        self.widthCheckBox.setText(_translate("Dialog", "PNG, width ="))
