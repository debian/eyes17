EYES17
======

**eyes17** is a program to control interactively most features of the
ExpEYES-17 box.

![expeyes-17 box, used with a small laptop](helpFiles/pics/eyes17-nb.jpg)

Eyes17 is from the PHOENIX project of Inter-University Accelerator
Centre, New Delhi. It is a hardware & software framework for developing
science experiments, demonstrations and projects and learn science and
engineering by exploration. 
 
- Capable of doing real time measurements and analysing the data in
  different ways.
- Analog voltages are measured with 0.025% resolution and time
   intervals with one microsecond.
- This project is based on Free and Open Source software, mostly
   written in Python programming language.
- The hardware design is also open. 

When compared with previous [ExpEYES boxes](https://expeyes.in)
(Expeyes, Expeyes-Junior), ExpEYES-17 is richer in various features:
multiple soft input ranges, more flexible wave generator, better time
resolution, better voltage resolution, richer in various features:
multiple soft input ranges, more flexible wave generator, better time
resolution, better voltage resolution, supports seamlessly cheap I2C
sensors.
